import os

from requests import request
from starlette.staticfiles import StaticFiles
from fastapi import FastAPI, UploadFile, File, HTTPException
from fastapi.responses import StreamingResponse
import onnxruntime as ort
import numpy as np
import io
import requests
from db.tortoise_models import ProcessedImage
from tortoise.contrib.fastapi import register_tortoise
from PIL import Image
from fastapi.templating import Jinja2Templates


app = FastAPI()

templates = Jinja2Templates(directory="templates")


# Загрузка моделей
session = ort.InferenceSession("./inswapper_128_fp16.onnx")

input_names = [input.name for input in session.get_inputs()]


# Функция для загрузки изображения по ссылке
def load_image_from_url(url):
    response = requests.get(url)
    image = Image.open(io.BytesIO(response.content)).convert('RGB')
    return image


def process_image(original_image_obj, target_image_obj):
    try:
        # Изменение размеров изображений
        original_image_resized = original_image_obj.resize((128, 128))
        target_image_resized = target_image_obj.resize((128, 128))

        # Преобразование изображений в формат, необходимый для модели
        original_image = np.array(original_image_resized).astype(np.float32)
        original_image = np.transpose(original_image, (2, 0, 1))
        original_image = np.expand_dims(original_image, axis=0) / 255.0  # Добавляем ось для батча и нормализуем

        target_image = np.array(target_image_resized).astype(np.float32)
        target_image = np.transpose(target_image, (2, 0, 1))
        target_image = np.expand_dims(target_image, axis=0) / 255.0  # Добавляем ось для батча и нормализуем

        # Проверка размеров изображений
        if original_image.shape != (1, 3, 128, 128) or target_image.shape != (1, 3, 128, 128):
            raise ValueError(f"Invalid image shapes. Expected (1, 3, 128, 128). Got {original_image.shape} and {target_image.shape}.")

        # Замена лица
        ort_inputs = {session.get_inputs()[0].name: original_image, session.get_inputs()[1].name: target_image}
        ort_outs = session.run(None, ort_inputs)

        # Обработка результата
        result_image_array = ort_outs[0][0] * 255  # Удаляем ось батча и возвращаем к диапазону [0, 255]
        result_image = Image.fromarray(result_image_array.astype(np.uint8), 'RGB')

        return result_image
    except Exception as e:
        print("Error occurred during image processing:", e)
        raise e


@app.post("/replace_face/")
async def replace_face(original_image_file: UploadFile = File(...), target_image_file: UploadFile = File(...)):
    try:
        # Открытие изображений
        original_image_obj = Image.open(original_image_file.file)
        target_image_obj = Image.open(target_image_file.file)

        # Изменение размеров изображений
        original_image_obj = original_image_obj.resize((128, 128))
        target_image_obj = target_image_obj.resize((128, 128))

        # Обработка изображений
        result_image = process_image(original_image_obj, target_image_obj)

        # Возвращение изображения в ответе
        if result_image is not None:
            result_image_bytes = io.BytesIO()
            result_image.save(result_image_bytes, format="PNG")
            result_image_bytes.seek(0)
            return StreamingResponse(result_image_bytes, media_type="image/png")
        else:
            return {"message": "Image processing failed."}
    except Exception as e:
        return {"message": f"Image processing failed. Error: {str(e)}"}


app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/result/{processed_image_id}/")
async def view_result(processed_image_id: int):
    processed_image = await ProcessedImage.get_or_none(id=processed_image_id)
    if not processed_image:
        raise HTTPException(status_code=404, detail="Обработанное изображение не найдено.")

    context = {"request": request, "processed_image": processed_image.processed_image}
    return templates.TemplateResponse("result.html", context)

# Инициализация базы данных
register_tortoise(
    app,
    db_url="sqlite://db.sqlite3",
    modules={"models": ["db.tortoise_models"]},
    generate_schemas=True,
)

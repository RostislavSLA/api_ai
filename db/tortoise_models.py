from tortoise.models import Model
from tortoise import fields
from tortoise.contrib.fastapi import register_tortoise


class ProcessedImage(Model):
    id = fields.IntField(pk=True)
    original_image_path = fields.CharField(max_length=255)
    target_image_path = fields.CharField(max_length=255)
    processed_image_path = fields.CharField(max_length=255)
